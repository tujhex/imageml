from matplotlib import pyplot as plt
import skimage
from skimage import data, io, measure
from skimage.feature import blob_dog, blob_log, blob_doh
from skimage.color import rgb2gray
from skimage.filters import rank
from skimage.filters.rank import entropy
from skimage.morphology import disk, watershed, black_tophat, closing, opening
from skimage.util import img_as_ubyte
from skimage.restoration import denoise_bilateral, denoise_tv_bregman

from math import sqrt
from scipy import ndimage
import os


# Этот кусок кода работает на мое машине
#folder = 'examples/'
folder = ''
examples = [
    'ibs_1.png',
    'ibs_2.png',
    'test1.jpg',
    'test3.jpg'
]


filename = # Тут должно быть имя файла #examples[2]
# Открытие файла (в чернобелом варианте), этот кусок кода стоит заменить
camera = io.imread(folder + filename, as_grey=True)
'''
def wtshed(camera):
    image = img_as_ubyte(camera)

    # denoise image
    denoised = rank.median(image, disk(2))

    # find continuous region (low gradient) --> markers
    markers = rank.gradient(denoised, disk(5)) < 10
    markers = ndimage.label(markers)[0]

    #local gradient
    gradient = rank.gradient(denoised, disk(2))

    # process the watershed
    labels = watershed(gradient, markers)

    # display results
    fig, axes = plt.subplots(ncols=4, figsize=(8, 2.7))
    ax0, ax1, ax2, ax3 = axes

    ax0.imshow(image, cmap=plt.cm.gray, interpolation='nearest')
    ax1.imshow(gradient, cmap=plt.cm.spectral, interpolation='nearest')
    ax2.imshow(markers, cmap=plt.cm.spectral, interpolation='nearest')
    ax3.imshow(image, cmap=plt.cm.gray, interpolation='nearest')
    ax3.imshow(labels, cmap=plt.cm.spectral, interpolation='nearest', alpha=.7)

    for ax in axes:
        ax.axis('off')

    fig.subplots_adjust(hspace=0.01, wspace=0.01, top=1, bottom=0, left=0, right=1)
    plt.show()
'''
# Почти стандартное использование энтропии (пример scikit-image)
def entropyi(camera):
    # help at http://scikit-image.org/docs/dev/api/skimage.morphology.html
    # Если использовать opening и closing то картинку тоже размывает, однако ее качество резко падает
    #camera = closing(camera) # dilation followed by an erosion
    #camera = opening(camera) # erosion followed by a dilation
    # Размытие, пока понравилось два варианта
    camera = denoise_bilateral(camera, sigma_spatial=15.2) #denoise using bilaterial
    #camera = denoise_tv_bregman(camera, 2) #denoise using total variation /bregman

    #camera = denoise_bilateral(camera, sigma_spatial=15.2) #denoise using bilaterial
    #io.imsave(folder + 'dn_' + filename, camera)
    image = img_as_ubyte(camera)

    fig, (ax0, ax1) = plt.subplots(ncols=2, figsize=(10, 4))

    img0 = ax0.imshow(image, cmap=plt.cm.gray)
    ax0.set_title('Image')
    ax0.axis('off')
    fig.colorbar(img0, ax=ax0)
    eimg = entropy(image, disk(5))

    # Фильтрация по значениям в массиве - оставляю только значения больше x
    for b in range(len(eimg)):
        for a in range(len(eimg[b])):
            if eimg[b][a] < 4.5:

                eimg[b][a] = 0


    img1 = ax1.imshow(eimg, cmap=plt.cm.jet)
    ax1.set_title('Entropy')
    ax1.axis('off')
    fig.colorbar(img1, ax=ax1)

    plt.show()
'''
def blobs(camera):
    image = camera
    image_gray = rgb2gray(image)

    blobs_log = blob_log(image_gray, min_sigma=2, max_sigma=20, num_sigma=10, threshold=.1)
    # Compute radii in the 3rd column.
    blobs_log[:, 2] = blobs_log[:, 2] * sqrt(2)

    blobs_dog = blob_dog(image_gray, min_sigma=2, max_sigma=20, threshold=.1)
    blobs_dog[:, 2] = blobs_dog[:, 2] * sqrt(2)

    blobs_doh = blob_doh(image_gray, min_sigma=2, max_sigma=20, threshold=.01)

    blobs_list = [blobs_log, blobs_dog, blobs_doh]
    colors = ['yellow', 'lime', 'red']
    titles = ['Laplacian of Gaussian', 'Difference of Gaussian','Determinant of Hessian']
    sequence = zip(blobs_list, colors, titles)


    fig,axes = plt.subplots(1, 3, sharex=True, sharey=True, subplot_kw={'adjustable':'box-forced'})
    axes = axes.ravel()
    for blobs, color, title in sequence:
        ax = axes[0]
        axes = axes[1:]
        ax.set_title(title)
        ax.imshow(image, interpolation='nearest')
        for blob in blobs:
            y, x, r = blob
            c = plt.Circle((x, y), r, color=color, linewidth=1, fill=False)

            ax.add_patch(c)

    plt.show()
'''
entropyi(camera)
